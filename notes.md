Завдання 2.1.
1-Встановлюємо Git та Visual Studio Code (VSC) на ПК
-	Налаштовуємо особисті дані у Gitbush
-	git config --global user.name "Vadym Teterin"
-	git config --global user.email "expertif@gmail.com

2-На https//Gitlab.com/Vadym-T створюємо проект «homework1-1» (public, інші «галочки» знімаємо)
3-Створюємо папку локального репозиторію «hw1-1»
4-Відкриваємо «hw1-1» за допомогою контекстного меню VSC
-	Створюємо файл index.htlm
-	VSC: Відкриваємо Термінал вводимо наступні команди:
-	git init – ініціалізація проекту під Git
-	git status – перевірка статусу/ бачимо що файли не відслідковуються
-	git add .  – додаєм файли у відслідковування
-	git status – перевірка статусу/ бачимо що файли відслідковуються
-	git remote add origin http://gitlab.com /Vadym-T/homework1-1  - прив’язка локального репозиторію з віддаленим на Gitlab.com
-	git commit  –a  -m “hw1-1 23-01-23’  комітимо файли 
-	git switch --create main – перейменовуємо гілку master -> main
-	git push origin main -  відправка файлів на віддалений репозиторій
-	git status – перевірка статусу

Завдання 2.2 
1-На https//Gitlab.com/Vadym-T створюємо проект «homework1-2» (public, інші «галочки» знімаємо)
2-Відкриваємо папку «hw1-2» за допомогою контекстного меню VSC
-	Створюємо файл index.htlm
-	Створюємо папку css
-	В папці сss створюємо файл 
-	VSC: Відкриваємо Термінал вводимо наступні команди:
-	git init – ініціалізація проекту під Git
-	git status – перевірка статусу/ бачимо що файли не відслідковуються
-	git add .  – додаєм файли у відслідковування
-	git status – перевірка статусу/ бачимо що файли відслідковуються
-	git remote add origin http://gitlab.com /Vadym-T/homework1-2  - прив’язка локального репозиторію з віддаленим на Gitlab.com
-	git commit  –a  -m “hw1-2  24-01-24’  комітимо файли 
-	git switch --create main – перейменовуємо гілку master -> main
-	git push origin main -  відправка файлів на віддалений репозиторій
-	git status – перевірка статусу
